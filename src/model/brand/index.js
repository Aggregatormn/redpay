const Sequelize = require('sequelize')
const sequelize = require('..')

const Model = Sequelize.Model
class Brands extends Model {}

Brands.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  brand_name: {
    type: Sequelize.STRING
  }
}, {
  sequelize,
  timestamps: false,
  modelName: 'brands'
})

module.exports = Brands
