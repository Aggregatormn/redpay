let successResponse = {
  invoice_number: '',
  link: '',
  response_type: ''
}
let errorResponse = {
  code: '',
  message: '',
  from: '',
  response_type: 'error'
}
let detailResponse = {
  invoice_number: '',
  status: 'not_paid',
  message: '',
  error_message: '',
  from: ''
}
let cancelResponse = {
  message: ''
}
module.exports = {
  criticErrorHandler: function (err, type) {
    errorResponse = {}
    errorResponse.from = type
    switch (type) {
      case 'lendmn':
        errorResponse.code = 'error_code_' + err.response.status
        errorResponse.message = err.response.statusText
        break
      case 'khaan_bank':
        errorResponse.code = 'error_code_' + err.response.status
        errorResponse.message = err.response.statusText
        break
      case 'qpay':
        errorResponse.message = err.response.data
        break
      case 'mongolchat':
        errorResponse.message = err
        break
      default:
        break
    }
    return errorResponse
  },
  errorHandler: function (err, type, statusCode, statusText) {
    errorResponse = {}
    errorResponse.from = type
    switch (type) {
      case 'lendmn':
        errorResponse.code = 'lendmn_code_' + err.code
        try {
          if (Object.prototype.hasOwnProperty.call(err, 'response')) {
            if (Object.prototype.hasOwnProperty.call(err, 'error_description')) {
              errorResponse.error_message = err.response.error_description
            }
          }
        } catch (error) {
          console.log(error)
        }
        switch (parseInt(err.code)) {
          case 1:
            errorResponse.message = 'parameter дутуу явуулсан тохиолдолд гарах алдаа.'
            break
          case 2:
            errorResponse.message = 'parameter алдаатай явуулсан тохиолдолд гарах алдаа.'
            break
          case 3:
            errorResponse.message = 'Хүсэлт илгээсэн сервис хараахан идэвхижээгүй үед.'
            break
          case 4:
            errorResponse.message = 'Invalid response.'
            break
          case 5:
            errorResponse.message = 'Тохиргоо буруу хийгдсэн.'
            break
          case 6:
            errorResponse.message = 'Мерчантийн хэтэвч үүсээгүй, идэвхигүй үед.'
            break
          case 57:
            errorResponse.message = 'Гүйлгээ олдсонгүй.'
            break
          case 58:
            errorResponse.message = 'Үлдэгдэл хүрэлцсэнгүй.'
            break
          case 59:
            errorResponse.message = 'Хэтэвчний өдрийн хэрэглээний лимит хэтэрсэн.'
            break
          case 60:
            errorResponse.message = 'Гүйлгээ хийгдсэн. Дахин хийх боломжгүй.'
            break
          case 61:
            errorResponse.message = 'Гүйлгээний доод хэмжээнээс бага мөнгө шилжүүлж болохгүй.'
            break
          case 62:
            errorResponse.message = 'Гүйлгээний дээд хэмжээнээс хэтэрсэн мөнгө шилжүүлж болохгүй.'
            break
          case 126:
            errorResponse.message = 'Нэхэмжлэл олдсонгүй.'
            break
          case 127:
            errorResponse.message = 'Нэхэмжлэлийн дүнг өөрчлөх боломжгүй.'
            break
          case 128:
            errorResponse.message = 'Нэхэмжлэл төлөгдсөн.'
            break
          case 130:
            errorResponse.message = 'Нэхэмжлэл цуцлагдсан.'
            break
          case 131:
            errorResponse.message = 'Нэхэмжлэлийн хүчинтэй хугацаа дууссан.'
            break
          case 132:
            errorResponse.message = 'Нэхэмжлэлийн үнийн дүн зөрөөтэй.'
            break
          case 201:
            errorResponse.message = 'Хэрэглэгч олдсонгүй'
            break
          case 400:
            errorResponse.message = 'Хүсэлтийн parameter буруу явуулсан.'
            break
          case 401:
            errorResponse.message = 'Буруу, хугацаа дууссан токен эсвэл тухайн үйлдлийг хийхэд permission хүрэхгүй үед.'
            break
        }
        break
      case 'khaan_bank':
        errorResponse.code = 'khaan_bank_code_' + err.errorCode
        switch (parseInt(err.errorCode)) {
          case 1:
            errorResponse.message = 'Order with this number is already registered in the system.'
            break
          case 3:
            errorResponse.message = 'Unknown or forbidden currency.'
            break
          case 4:
            errorResponse.message = 'Mandatory request parameter was not specified.'
            break
          case 5:
            errorResponse.message = 'Erroneous value of a request parameter.'
            break
          case 7:
            errorResponse.message = 'System error.'
            break
          default:
            errorResponse.message = 'Unknown'
            errorResponse.code = statusCode
        }
        break
      case 'qpay':
        errorResponse.code = err
        break
      default:
        break
    }
    return errorResponse
  },
  successHandler: function (data, type, brandName) {
    successResponse = {}
    switch (type) {
      case 'lendmn':
        if (data.code === 0) {
          successResponse.invoice_number = data.response.invoiceNumber
          successResponse.qr_link = data.response.qr_link
          successResponse.qr_string = data.response.qr_string
          successResponse.amount = data.response.amount
          // resp['response_type'] = 'created'
        }
        break
      case 'khaan_bank':
        if (data.errorCode === 0) {
          successResponse.invoice_number = data.orderId
          successResponse.link = data.formUrl
          successResponse.status = 'created'
        }
        break
      case 'qpay':
        if (data.payment_id) {
          successResponse = data
        }
        break
      case 'monpay':
        successResponse = data
        break
      default:
        break
    }
    return successResponse
  },
  detailHandler: function (data, type) {
    detailResponse = {}
    switch (type) {
      case 'lendmn':
        detailResponse.invoice_number = data.response.invoiceNumber
        detailResponse.from = type
        switch (parseInt(data.code)) {
          case 0:
            detailResponse.description = data.response.description
            detailResponse.amount = data.response.amount
            detailResponse.trackingData = data.response.trackingData
            detailResponse.createdAt = data.response.createdAt
            detailResponse.expireDate = data.response.expireDate
            switch (parseInt(data.response.status)) {
              case 0:
                detailResponse.message = 'Хүлээгдэж байгаа'
                detailResponse.status = 'waiting'
                break
              case 1:
                detailResponse.message = 'Төлөгдсөн'
                detailResponse.status = 'paid'
                detailResponse.paid_date = data.response.paidDate
                break
              case 2:
                detailResponse.message = 'Цуцлагдсан'
                detailResponse.status = 'cancelled'
                break
              case 3:
                detailResponse.message = 'Хугацаа нь дууссан'
                detailResponse.status = 'exprired'
                break
              default:
                detailResponse.message = 'Хүсэлт амжилттай биелсэн.'
                break
            }
            break
          case 1:
            detailResponse.message = 'parameter дутуу явуулсан тохиолдолд гарах алдаа.'
            detailResponse.status = 'error'
            break
          case 2:
            detailResponse.message = 'parameter алдаатай явуулсан тохиолдолд гарах алдаа.'
            detailResponse.status = 'error'
            break
          case 3:
            detailResponse.message = 'Хүсэлт илгээсэн сервис хараахан идэвхижээгүй үед.'
            detailResponse.status = 'error'
            break
          case 4:
            detailResponse.message = 'Invalid response.'
            detailResponse.status = 'error'
            break
          case 5:
            detailResponse.message = 'Тохиргоо буруу хийгдсэн.'
            detailResponse.status = 'error'
            break
          case 6:
            detailResponse.message = 'Мерчантийн хэтэвч үүсээгүй, идэвхигүй үед.'
            detailResponse.status = 'error'
            break
          case 57:
            detailResponse.message = 'Гүйлгээ олдсонгүй.'
            detailResponse.status = 'error'
            break
          case 58:
            detailResponse.message = 'Үлдэгдэл хүрэлцсэнгүй.'
            detailResponse.status = 'error'
            break
          case 59:
            detailResponse.message = 'Хэтэвчний өдрийн хэрэглээний лимит хэтэрсэн.'
            detailResponse.status = 'error'
            break
          case 60:
            detailResponse.message = 'Гүйлгээ хийгдсэн. Дахин хийх боломжгүй.'
            detailResponse.status = 'error'
            break
          case 61:
            detailResponse.message = 'Гүйлгээний доод хэмжээнээс бага мөнгө шилжүүлж болохгүй.'
            detailResponse.status = 'error'
            break
          case 62:
            detailResponse.message = 'Гүйлгээний дээд хэмжээнээс хэтэрсэн мөнгө шилжүүлж болохгүй.'
            detailResponse.status = 'error'
            break
          case 126:
            detailResponse.message = 'Нэхэмжлэл олдсонгүй.'
            detailResponse.status = 'error'
            break
          case 127:
            detailResponse.message = 'Нэхэмжлэлийн дүнг өөрчлөх боломжгүй.'
            detailResponse.status = 'error'
            break
          case 128:
            detailResponse.message = 'Нэхэмжлэл төлөгдсөн.'
            detailResponse.status = 'paid'
            break
          case 130:
            detailResponse.message = 'Нэхэмжлэл цуцлагдсан.'
            detailResponse.status = 'cancelled'
            break
          case 131:
            detailResponse.message = 'Нэхэмжлэлийн хүчинтэй хугацаа дууссан.'
            detailResponse.status = 'error'
            break
          case 132:
            detailResponse.message = 'Нэхэмжлэлийн үнийн дүн зөрөөтэй.'
            detailResponse.status = 'error'
            break
          case 201:
            detailResponse.message = 'Хэрэглэгч олдсонгүй'
            detailResponse.status = 'error'
            break
          case 400:
            detailResponse.message = 'Хүсэлтийн parameter буруу явуулсан.'
            detailResponse.status = 'error'
            break
          case 401:
            detailResponse.message = 'Буруу, хугацаа дууссан токен эсвэл тухайн үйлдлийг хийхэд permission хүрэхгүй үед.'
            detailResponse.status = 'error'
            break
        }
        break
      case 'khaan_bank':
        detailResponse.invoice_number = data.invoice_number
        detailResponse.status = 'not_paid'
        detailResponse.from = type
        switch (data.OrderStatus) {
          case 0:
            detailResponse.message = 'Order registered, but not paid off.'
            detailResponse.status = 'waiting'
            break
          case 1:
            detailResponse.message = 'Pre-authorisation of order amount was held (for two-stage payment)'
            detailResponse.status = 'waiting'
            break
          case 2:
            detailResponse.status = 'paid'
            detailResponse.message = 'Order amount is fully authorized.'
            break
          case 3:
            detailResponse.message = 'Authorization canceled.'
            break
          case 4:
            detailResponse.message = 'Transaction was refunded.'
            break
          case 5:
            detailResponse.message = 'Initiated authorization using ACS of the issuer bank.'
            break
          case 6:
            detailResponse.message = 'Authorization declined.'
            break
          default:
            detailResponse.message = ''
            break
        }
        switch (parseInt(data.ErrorCode)) {
          case 0:
            detailResponse.error_message = 'No system error.'
            break
          case 2:
            detailResponse.error_message = 'The order is declined because of an error in the payment credentials.'
            break
          case 5:
            detailResponse.error_message = 'Erroneous value of a request parameter.'
            break
          case 6:
            detailResponse.error_message = 'Unregistered OrderId.'
            break
        }
        break
      case 'qpay':
        detailResponse = {
          status: data.payment_info.payment_status,
          transaction_id: data.payment_info.transaction_id,
          invoice_number: data.bill_no
        }
        break
      default:
        break
    }
    return detailResponse
  },
  cancelHandler: function (data, type) {
    cancelResponse = {}
    switch (type) {
      case 'khaan_bank':
        break
      case 'lendmn':
        cancelResponse.from = type
        switch (parseInt(data.code)) {
          case 0:
            cancelResponse.message = 'Амжилттай цуцлалаа'
            break
          case 1:
            cancelResponse.message = 'parameter дутуу явуулсан тохиолдолд гарах алдаа.'
            cancelResponse.status = 'error'
            break
          case 2:
            cancelResponse.message = 'parameter алдаатай явуулсан тохиолдолд гарах алдаа.'
            cancelResponse.status = 'error'
            break
          case 3:
            cancelResponse.message = 'Хүсэлт илгээсэн сервис хараахан идэвхижээгүй үед.'
            cancelResponse.status = 'error'
            break
          case 4:
            cancelResponse.message = 'Invalid response.'
            cancelResponse.status = 'error'
            break
          case 5:
            cancelResponse.message = 'Тохиргоо буруу хийгдсэн.'
            cancelResponse.status = 'error'
            break
          case 6:
            cancelResponse.message = 'Мерчантийн хэтэвч үүсээгүй, идэвхигүй үед.'
            cancelResponse.status = 'error'
            break
          case 57:
            cancelResponse.message = 'Гүйлгээ олдсонгүй.'
            cancelResponse.status = 'error'
            break
          case 58:
            cancelResponse.message = 'Үлдэгдэл хүрэлцсэнгүй.'
            cancelResponse.status = 'error'
            break
          case 59:
            cancelResponse.message = 'Хэтэвчний өдрийн хэрэглээний лимит хэтэрсэн.'
            cancelResponse.status = 'error'
            break
          case 60:
            cancelResponse.message = 'Гүйлгээ хийгдсэн. Дахин хийх боломжгүй.'
            cancelResponse.status = 'error'
            break
          case 61:
            cancelResponse.message = 'Гүйлгээний доод хэмжээнээс бага мөнгө шилжүүлж болохгүй.'
            cancelResponse.status = 'error'
            break
          case 62:
            cancelResponse.message = 'Гүйлгээний дээд хэмжээнээс хэтэрсэн мөнгө шилжүүлж болохгүй.'
            cancelResponse.status = 'error'
            break
          case 126:
            cancelResponse.message = 'Нэхэмжлэл олдсонгүй.'
            cancelResponse.status = 'error'
            break
          case 127:
            cancelResponse.message = 'Нэхэмжлэлийн дүнг өөрчлөх боломжгүй.'
            cancelResponse.status = 'error'
            break
          case 128:
            cancelResponse.message = 'Нэхэмжлэл төлөгдсөн.'
            cancelResponse.status = 'paid'
            break
          case 130:
            cancelResponse.message = 'Нэхэмжлэл цуцлагдсан.'
            cancelResponse.status = 'cancelled'
            break
          case 131:
            cancelResponse.message = 'Нэхэмжлэлийн хүчинтэй хугацаа дууссан.'
            cancelResponse.status = 'error'
            break
          case 132:
            cancelResponse.message = 'Нэхэмжлэлийн үнийн дүн зөрөөтэй.'
            cancelResponse.status = 'error'
            break
          case 201:
            cancelResponse.message = 'Хэрэглэгч олдсонгүй'
            cancelResponse.status = 'error'
            break
          case 400:
            cancelResponse.message = 'Хүсэлтийн parameter буруу явуулсан.'
            cancelResponse.status = 'error'
            break
          case 401:
            cancelResponse.message = 'Буруу, хугацаа дууссан токен эсвэл тухайн үйлдлийг хийхэд permission хүрэхгүй үед.'
            cancelResponse.status = 'error'
            break
        }
        break
    }
    return cancelResponse
  }
}
