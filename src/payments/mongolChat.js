const axios = require('axios')
const responseHandler = require('../responseHandlerNew.js')
const Invoice = require('../model/invoice')
const responseHandlerNew = require('../responseHandlerNew.js')
module.exports = {
  createInvoice: async function (data, credential) {
    const headers = {
      'Content-Type': 'application/json',
      Authorization: credential.Authorization,
      'api-key': credential.apiKey
    }
    try {
      const retInvoice = await Invoice.findOne({ where: { invoice_number: data.trackingData, payment_name: data.payment_name, brand_name: data.brand_name } })
      const body = {
        amount: data.amount,
        products: [
          {
            product_name: 'ТокТок хүргэлт',
            quantity: '1',
            price: data.amount
          }
        ],
        title: 'ТОКТОК хүргэлт',
        sub_title: 'Бүх төрлийн хүргэлтийн үйлчилгээ',
        noat: '0',
        nhat: '0',
        ttd: '6183352',
        reference_number: data.trackingData,
        branch_id: 'TOKTOK'
      }
      if (retInvoice) {
        const resAxios = await axios.post(credential.url + 'onlineqr/status', { qr: retInvoice.qr_code }, { headers: headers })
        if (resAxios.data.status === 'expired' || retInvoice.amount !== data.amount) {
          const resAxiosInvoice = await axios.post(credential.url + 'onlineqr/generate', body, { headers: headers })
          Invoice.update({
            payment_id: data.trackingData,
            amount: data.amount,
            brand_name: data.brand_name,
            payment_name: data.payment_name,
            invoice_number: data.trackingData,
            description: data.description,
            qr_code: resAxiosInvoice.data.qr,
            invoice_url: null,
            status: 'created'
          },
          { where: { id: retInvoice.id } })
          const temp = {
            payment_id: data.trackingData,
            amount: data.amount,
            brand_name: data.brand_name,
            payment_name: data.payment_name,
            invoice_number: data.trackingData,
            description: data.description,
            qr_code: resAxiosInvoice.data.qr,
            invoice_url: null,
            status: 'created'
          }
          temp.id = retInvoice.id
          return temp
        } else {
          return retInvoice
        }
      } else {
        try {
          const resAxios = await axios.post(credential.url + 'onlineqr/generate', body, { headers: headers })
          const invoice = await Invoice.create({
            payment_id: data.trackingData,
            amount: data.amount,
            brand_name: data.brand_name,
            payment_name: data.payment_name,
            invoice_number: data.trackingData,
            description: data.description,
            qr_code: resAxios.data.qr,
            invoice_url: null,
            status: 'created'
          })
          return invoice
        } catch (err) {
          if (err.response) {
            return responseHandlerNew.criticErrorHandler(err.response.data.message, data.payment_name)
          } else {
            return responseHandlerNew.criticErrorHandler(err.message, data.payment_name)
          }
        }
      }
    } catch (error) {
      return error.message
    }
  },
  detailInvoice: async function (data, credential) {
    const headers = {
      'Content-Type': 'application/json',
      Authorization: credential.Authorization,
      'api-key': credential.apiKey
    }
    try {
      const retInvoice = await Invoice.findOne({ where: { payment_id: data.invoice_number, payment_name: data.payment_name, brand_name: data.brand_name } })
      if (retInvoice) {
        const resAxios = await axios.post(credential.url + 'onlineqr/status', { qr: retInvoice.qr_code }, { headers: headers })
        Invoice.update({
          status: resAxios.data.status.toUpperCase()
        },
        { where: { id: retInvoice.id } })
        return resAxios.data
      } else {
        return responseHandler.successHandler('нэхэмжлэх олдсонгүй', data.payment_name, data.brand_name)
      }
    } catch (error) {
      return error.message
    }
  }
}
