const Sequelize = require('sequelize')
const sequelize = require('..')

const Model = Sequelize.Model
class PaymentType extends Model {}

PaymentType.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  payment_name: {
    type: Sequelize.STRING
  },
  name_mon: {
    type: Sequelize.STRING
  },
  name_eng: {
    type: Sequelize.STRING
  }
}, {
  sequelize,
  timestamps: false,
  modelName: 'payment_types'
})

module.exports = PaymentType
