const Sequelize = require('sequelize')
const sequelize = require('..')

const Model = Sequelize.Model
class Invoice extends Model {}

Invoice.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  brand_name: {
    type: Sequelize.STRING
  },
  payment_name: {
    type: Sequelize.STRING
  },
  invoice_number: {
    type: Sequelize.STRING
  },
  payment_id: {
    type: Sequelize.STRING
  },
  amount: {
    type: Sequelize.NUMBER
  },
  invoice_url: {
    type: Sequelize.STRING
  },
  qr_code: {
    type: Sequelize.STRING
  },
  status: {
    type: Sequelize.STRING
  },
  access_token: {
    type: Sequelize.STRING
  },
  finished_at: {
    type: Sequelize.DATE
  },
  description: {
    type: Sequelize.STRING
  }
}, {
  sequelize,
  tableName: 'invoice',
  timestamps: false,
  modelName: 'invoice'
})

module.exports = Invoice
