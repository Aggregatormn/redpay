const axios = require('axios')
const responseHandler = require('../responseHandlerNew')

const headers = {
  'Content-Type': 'application/json',
  'Access-Control-Allow-Origin': '*'
}

module.exports = {
  createInvoice: async function (data, credential) {
    const randomNumber = parseInt((Math.random() * (99999999 - 0) + 0), 10)
    const orderNumberGenerated = getOrderNumber() + '' + randomNumber
    const url = 'https://epp.khanbank.com/payment/rest/register.do?' +
            'userName=' + credential.username +
            '&password=' + credential.password +
            '&amount=' + data.amount +
            '&sessionTimeoutSecs=' + credential.invoiceDuration +
            '&returnUrl=' + credential.redirectUrl +
            '&failUrl=' + credential.failUrl +
            '&currency=' + 496 +
            '&orderNumber=' + orderNumberGenerated +
            '&jsonParams={"orderNumber":' + orderNumberGenerated + '}'
    // if (data.hasOwnProperty('page_view')) {
    //     url += '&pageView=' + data.page_view
    // }
    const promise = new Promise((resolve, reject) => {
      axios.post(url, { headers: headers }).then((res) => {
        if (res.data.errorCode === 0) {
          resolve(responseHandler.successHandler(res.data, data.payment_name, data.brand_name))
        } else {
          resolve(responseHandler.errorHandler(res.data, data.payment_name, res.status, res.statusText))
        }
      }).catch((err) => {
        console.log(err)
        reject(responseHandler.criticErrorHandler(err, data.payment_name))
      })
    })
    const result = await promise
    return result
  },
  detailInvoice: async function (data, credential) {
    // let invoice_number = JSON.parse(data.payload).invoice_number
    const promise = new Promise((resolve, reject) => {
      axios.get('https://epp.khanbank.com/payment/rest/getOrderStatus.do?' +
                'orderId=' + data.invoice_number +
                '&password=' + credential.password +
                '&userName=' + credential.username, { headers: headers }).then((res) => {
        const handlerData = res.data
        handlerData.invoice_number = data.invoice_number
        // console.log(res.data)
        resolve(responseHandler.detailHandler(handlerData, data.payment_name))
      }).catch((err) => {
        reject(responseHandler.errorHandler(err, data.type))
      })
    })
    const result = await promise
    return result
  }
}

function getOrderNumber () {
  const date = new Date()
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hours = date.getHours()
  const minutes = date.getMinutes()
  const seconds = date.getSeconds()
  const orderNumber = year + '' + month + '' + day + '' + hours + '' + minutes + '' + seconds
  return orderNumber
}
