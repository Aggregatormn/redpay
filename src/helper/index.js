const Brand = require('../model/brand')
const PaymentList = require('../model/paymentList')
const PaymentType = require('../model/paymentType')
const Op = require('sequelize').Op
const exp = module.exports
const khan = require('../payments/khan')
const lendmn = require('../payments/lend')
const qpay = require('../payments/qpay')
const monpay = require('../payments/monpay')
const mongolChat = require('../payments/mongolChat')

exp.getPaymentByBrand = async function (brandName, res, response) {
  const brands = await Brand.findAll({
    where: {
      brand_name: brandName
    }
  })
  if (brands.length > 0) {
    const payments = await PaymentList.findAll({
      attributes: [
        'payment_name'
      ],
      where: {
        brand_name: brandName
      }
    })
    const fixedPayments = []
    payments.forEach(element => {
      fixedPayments.push(element.payment_name)
    })
    const paymentTypes = await PaymentType.findAll({
      attributes: ['payment_name', 'name_mon', 'name_eng'],
      where: {
        payment_name: { [Op.in]: fixedPayments }
      }
    })
    response.status = 'success'
    response.response.data.brand_name = brandName
    response.response.data.payment_list = paymentTypes
    res.json(response)
    res.end()
  } else {
    response.status = 'success'
    response.response.data.brand_name = brandName
    response.response.data.payment_list = []
    res.json(response)
    res.end()
  }
}
exp.createInvoiceNew = async function (payload, res, response) {
  const brands = await Brand.findAll(
    {
      where: {
        brand_name: payload.brand_name
      }
    })
  if (brands.length > 0) {
    genInvoiceByBrandName(payload, (err, resData) => {
      // console.log('genInvoiceByBrandName', resData)
      if (err) {
        response.status = 'failed'
        response.error = err
        res.json(response)
      } else {
        response.error = {}
        response.error.code = ''
        response.error.message = ''
        response.status = 'success'
        response.response.data = resData
        res.json(response)
      }
    })
  } else {
    const errorData = {}
    errorData.code = 'createInvoice_error_1'
    errorData.message = 'Бренд бүртгэгдээгүй эсвэл төлбөрийн төрөл боломжгүй'
    res.send(errorData)
  }
}
exp.detailInvoice = async function (payload, res, response) {
  const brands = await Brand.findAll(
    {
      where: {
        brand_name: payload.brand_name
      }
    })
  if (brands.length > 0) {
    getInvoiceDetail(payload, (err, resData) => {
      if (err) {
        response.status = 'failed'
        response.error = err
        res.json(response)
      } else {
        response.error = {}
        response.error.code = ''
        response.error.message = ''
        response.status = 'success'
        response.response.data = resData
        res.json(response)
      }
    })
  } else {
    const errorData = {}
    errorData.code = 'createInvoice_error_1'
    errorData.message = 'Бренд бүртгэгдээгүй эсвэл төлбөрийн төрөл боломжгүй'
    res.send(errorData)
  }
}
exp.cancelInvoice = async function (payload, result) {
  const brands = await Brand.findAll(
    {
      where: {
        brand_name: payload.brand_name
      }
    })
  if (brands.length > 0) {
    cancelInvoice(payload, (err, resData) => {
      console.log('cancelInvoice', err, resData)
      if (err) {
        result(err, null)
      } else {
        try {
          result(null, resData)
        } catch (error) {
          console.log('try', error)
        }
      }
    })
  } else {
    result('null', null)
  }
}
exp.getToken = async function (data, result) {
  const paymentList = await PaymentList.findAll({
    where: {
      brand_name: data.brand_name,
      payment_name: data.payment_name
    }
  })
  if (paymentList.length > 0) {
    let credential = {}
    const sqlData = paymentList[0]
    if (sqlData.payment_name === 'lendmn') {
      credential = JSON.parse(sqlData.config)
      lendmn.userInfo(data, credential).then((response) => {
        result(null, response)
      }).catch(error => {
        result(error, null)
      })
    } else {
      result('null', null)
    }
  } else {
    result('null', null)
  }
}
exp.userInfo = async function (data, result) {
  const paymentList = await PaymentList.findAll({
    where: {
      brand_name: data.brand_name,
      payment_name: data.payment_name
    }
  })
  if (paymentList.length > 0) {
    let credential = {}
    const sqlData = paymentList[0]
    if (sqlData.payment_name === 'lendmn') {
      credential = JSON.parse(sqlData.config)
      lendmn.userInfo(data, credential).then((response) => {
        result(null, response)
      }).catch(error => {
        result(error, null)
      })
    } else {
      result('null', null)
    }
  } else {
    result('null', null)
  }
}
const genInvoiceByBrandName = async function (data, result) {
  const paymentList = await PaymentList.findAll({
    where: {
      brand_name: data.brand_name,
      payment_name: data.payment_name
    }
  })
  if (paymentList.length > 0) {
    const sqlData = paymentList[0]
    let credential = {}
    switch (sqlData.payment_name) {
      case 'khaan_bank':
        credential = JSON.parse(sqlData.config)
        khan.createInvoice(data, credential).then((response) => {
          result(null, response)
        }).catch(error => {
          result({ error: error }, null)
        })
        break
      case 'lendmn':
        credential = JSON.parse(sqlData.config)
        lendmn.createInvoice(data, credential).then((response) => {
          result(null, response)
        }).catch(error => {
          result({ error: error }, null)
        })
        break
      case 'mongolchat':
        credential = JSON.parse(sqlData.config)
        mongolChat.createInvoice(data, credential).then((response) => {
          result(null, response)
        }).catch(error => {
          result({ error: error.message }, null)
        })
        break
      case 'monpay':
        credential = JSON.parse(sqlData.config)
        monpay.createInvoice(data, credential).then((response) => {
          result(null, response)
        }).catch(error => {
          result({ error: error.message }, null)
        })
        break
      case 'qpay':
        credential = JSON.parse(sqlData.config)
        qpay.createInvoice(data, credential).then((response) => {
          result(null, response)
        }).catch(error => {
          result({ error: error.message }, null)
        })
        break
    }
  } else {
    const errorData = {}
    errorData.code = 'createInvoice_error_2'
    errorData.message = 'Бренд бүртгэгдээгүй эсвэл төлбөрийн төрөл боломжгүй'
    result(errorData, null)
  }
}
const getInvoiceDetail = async function (data, result) {
  const paymentList = await PaymentList.findAll({
    where: {
      brand_name: data.brand_name,
      payment_name: data.payment_name
    }
  })
  if (paymentList.length > 0) {
    let credential = {}
    const sqlData = paymentList[0]
    switch (sqlData.payment_name) {
      case 'khaan_bank':
        credential = JSON.parse(sqlData.config)
        khan.detailInvoice(data, credential).then((response) => {
          result(null, response)
        }).catch(error => {
          result(error, null)
        })
        break
      case 'lendmn':
        credential = JSON.parse(sqlData.config)
        lendmn.detailInvoice(data, credential).then((response) => {
          result(null, response)
        }).catch(error => {
          result(error, null)
        })
        break
      case 'monpay':
        credential = JSON.parse(sqlData.config)
        monpay.detailInvoice(data, credential).then((response) => {
          result(null, response)
        }).catch(error => {
          result(error, null)
        })
        break
      case 'mongolchat':
        credential = JSON.parse(sqlData.config)
        mongolChat.detailInvoice(data, credential).then((response) => {
          result(null, response)
        }).catch(error => {
          result({ error: error.message }, null)
        })
        break
      case 'qpay':
        credential = JSON.parse(sqlData.config)
        qpay.detailInvoice(data, credential).then((response) => {
          result(null, response)
        }).catch(error => {
          result(error, null)
        })
        break
    }
  } else {
    result('null', null)
  }
}
const cancelInvoice = async function (data, result) {
  const paymentList = await PaymentList.findAll({
    where: {
      brand_name: data.brand_name,
      payment_name: data.payment_name
    }
  })
  if (paymentList.length > 0) {
    let credential = {}
    const err = {}
    const sqlData = paymentList[0]
    switch (sqlData.payment_name) {
      case 'khaan_bank':
        err.code = 'can_not_cancel_on_khaan_bank'
        err.message = 'Хаан банк дээр цуцлах үйлдэл хийх боломжгүй'
        err.from = 'payment_service'
        result(err, null)
        break
      case 'lendmn':
        credential = JSON.parse(sqlData.config)
        lendmn.cancelInvoice(data, credential).then((response) => {
          result(null, response)
        }).catch(error => {
          result(error, null)
        })
        break
    }
  } else {
    result('null', null)
  }
}
