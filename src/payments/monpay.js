const axios = require('axios')
const btoa = require('btoa')
const responseHandler = require('../responseHandlerNew.js')
const Invoice = require('../model/invoice')
module.exports = {
  createInvoice: async function (data, credential) {
    const token = btoa(credential.user_name + ':' + credential.password)
    const headers = {
      'Content-Type': 'application/json',
      Authorization: 'Basic ' + token
    }
    try {
      const resAxios = await axios.post(credential.url + 'generate', { amount: data.amount, displayName: 'TOKTOK', generateUuid: true }, { headers: headers })
      const invoice = await Invoice.create({
        payment_id: resAxios.data.result.uuid,
        amount: data.amount,
        brand_name: data.brand_name,
        payment_name: data.payment_name,
        invoice_number: data.trackingData,
        description: data.description,
        qr_code: resAxios.data.result.qrcode,
        invoice_url: null,
        status: 'created'
      })
      return invoice
    } catch (error) {
      return error.message
    }
  },
  detailInvoice: async function (data, credential) {
    const token = btoa(credential.user_name + ':' + credential.password)
    const headers = {
      'Content-Type': 'application/json',
      Authorization: 'Basic ' + token
    }
    try {
      const invoices = await Invoice.findAll({
        where: {
          payment_id: data.invoice_number
        }
      })
      if (invoices.length > 0) {
        const resAxios = await axios.get(credential.url + 'check?uuid=' + data.invoice_number, { headers: headers })
        if (resAxios.data.info === 'QR код уншуулсан байна') {
          Invoice.update({
            status: 'PAID'
          },
          { where: { id: invoices[0].id } })
          return resAxios.data
        } else {
          return resAxios.data
        }
      } else {
        return responseHandler.successHandler('нэхэмжлэх олдсонгүй', data.payment_name, data.brand_name)
      }
    } catch (error) {
      return error.message
    }
  }
}
