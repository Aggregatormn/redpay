const Sequelize = require('sequelize')
const sequelize = require('..')

const Model = Sequelize.Model
class PaymentList extends Model {}

PaymentList.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  brand_name: {
    type: Sequelize.STRING
  },
  payment_name: {
    type: Sequelize.STRING
  },
  config: {
    type: Sequelize.JSON
  }
}, {
  sequelize,
  tableName: 'payment_list',
  timestamps: false,
  modelName: 'payment_list'
})

module.exports = PaymentList
