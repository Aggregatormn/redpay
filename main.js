const express = require('express')
const bodyParser = require('body-parser')
const winston = require('winston')
const expressWinston = require('express-winston')
const { validationResult, body, query } = require('express-validator')
const cors = require('cors')
const Invoice = require('./src/model/invoice')
const PaymentList = require('./src/model/paymentList')
// eslint-disable-next-line
const DailyRotateFile = require('winston-daily-rotate-file')
const Helper = require('./src/helper')
const app = express()
const axios = require('axios')
const port = 3000
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cors())
const defResponse = {
  status: 'success',
  error: {
    code: '',
    message: ''
  },
  response: {
    data: {}
  }
}

app.use(expressWinston.logger({
  requestWhitelist: ['headers', 'query'],
  responseWhitelist: ['body'],
  transports: [
    new winston.transports.DailyRotateFile({
      name: 'file',
      datePattern: 'YYYY-MM-DD',
      filename: './logs/access-%DATE%.log',
      handleExceptions: true,
      level: 'info',
      json: false,
      colorize: false
    })
  ],
  exitOnError: false,
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.json()
  ),
  dynamicMeta: (req, res) => {
    const httpRequest = {}
    const meta = {}
    if (req) {
      meta.httpRequest = httpRequest
      httpRequest.requestMethod = req.method
      httpRequest.requestUrl = `${req.protocol}://${req.get('host')}${req.originalUrl}`
      httpRequest.protocol = `HTTP/${req.httpVersion}`
      // httpRequest.remoteIp = req.ip // this includes both ipv6 and ipv4 addresses separated by ':'
      httpRequest.remoteIp = req.ip.indexOf(':') >= 0 ? req.ip.substring(req.ip.lastIndexOf(':') + 1) : req.ip // just ipv4
      httpRequest.requestSize = req.socket.bytesRead
      httpRequest.userAgent = req.get('User-Agent')
      httpRequest.referrer = req.get('Referrer')
    }

    if (res) {
      meta.httpRequest = httpRequest
      httpRequest.status = res.statusCode
      httpRequest.latency = {
        seconds: Math.floor(res.responseTime / 1000),
        nanos: (res.responseTime % 1000) * 1000000
      }
      if (res.body) {
        if (typeof res.body === 'object') {
          httpRequest.responseSize = JSON.stringify(res.body).length
        } else if (typeof res.body === 'string') {
          httpRequest.responseSize = res.body.length
        }
      }
    }
    return meta
  }
}))

app.use(expressWinston.errorLogger({
  requestWhitelist: ['headers', 'query'],
  responseWhitelist: ['body'],
  dumpExceptions: true,
  transports: [
    new winston.transports.DailyRotateFile({
      name: 'file',
      datePattern: 'YYYY-MM-DD',
      filename: './logs/error-%DATE%.log',
      handleExceptions: true,
      level: 'warn',
      json: true,
      colorize: false
    })
  ],
  exitOnError: false,
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.json()
  ),
  dynamicMeta: (req, res) => {
    const httpRequest = {}
    const meta = {}
    if (req) {
      meta.httpRequest = httpRequest
      httpRequest.requestMethod = req.method
      httpRequest.requestUrl = `${req.protocol}://${req.get('host')}${req.originalUrl}`
      httpRequest.protocol = `HTTP/${req.httpVersion}`
      // httpRequest.remoteIp = req.ip // this includes both ipv6 and ipv4 addresses separated by ':'
      httpRequest.remoteIp = req.ip.indexOf(':') >= 0 ? req.ip.substring(req.ip.lastIndexOf(':') + 1) : req.ip // just ipv4
      httpRequest.requestSize = req.socket.bytesRead
      httpRequest.userAgent = req.get('User-Agent')
      httpRequest.referrer = req.get('Referrer')
    }

    if (res) {
      meta.httpRequest = httpRequest
      httpRequest.status = res.statusCode
      httpRequest.latency = {
        seconds: Math.floor(res.responseTime / 1000),
        nanos: (res.responseTime % 1000) * 1000000
      }
      if (res.body) {
        if (typeof res.body === 'object') {
          httpRequest.responseSize = JSON.stringify(res.body).length
        } else if (typeof res.body === 'string') {
          httpRequest.responseSize = res.body.length
        }
      }
    }
    return meta
  }
}))

app.get('/a', (req, res) => {
  res.send(new Date().getDate() + ' ')
})

app.get('/', (req, res) => {
  res.send('server alivee')
})
app.get('/getPaymentByBrand', [
  query('brand_name')
    .isString()
    .isLength({ min: 1 }).withMessage('must be at least 1 chars long')
    .isLength({ max: 30 }).withMessage('must be at greatest 30 chars long')
], async (req, res) => {
  const response = defResponse
  response.response.data = {
    brand_name: req.query.brand_name,
    payment_list: []
  }
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    response.status = 'failed'
    response.error.code = 'error_on_' + errors.array()[0].param
    response.error.message = errors
    response.response = {}
    return res.status(422).json(response)
  }
  try {
    Helper.getPaymentByBrand(req.query.brand_name, res, response)
  } catch (error) {
    response.status = 'failed'
    response.error.code = 'try_error'
    response.error.message = error.message
    response.response.data.payment_list = []
    res.json(response)
    res.end()
  }
})

app.post('/createInvoice2', [
  body('brand_name')
    .isString()
    .isLength({ min: 1 }).withMessage('must be at least 1 chars long')
    .isLength({ max: 30 }).withMessage('must be at greatest 30 chars long'),
  body('payment_name')
    .isString()
    .isLength({ min: 1 }).withMessage('must be at least 1 chars long')
    .isLength({ max: 30 }).withMessage('must be at greatest 30 chars long'),
  body('amount')
    .isInt().withMessage('value must be integer')
    .isLength({ min: 1 }).withMessage('must be at least 1 chars long')
    .isLength({ max: 30 }).withMessage('must be at greatest 30 chars long'),
  body('page_view')
    .optional()
    .isString()
    .isIn(['MOBILE', 'WEB']).withMessage('value must be \'MOBILE\' or \'WEB\' (Uppercase)')
],
(req, res) => {
  const response = defResponse
  response.response.data = {}
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    response.status = 'failed'
    response.error.from = 'payment_service'
    response.error.code = 'error_on_' + errors.array()[0].param
    response.error.message = errors
    response.response = {}
    return res.status(422).json(response)
  }
  Helper.createInvoiceNew(req.body, res, response)
}
)
app.post('/detailInvoice2', [
  body('brand_name')
    .isString()
    .isLength({ min: 1 }).withMessage('must be at least 1 chars long')
    .isLength({ max: 30 }).withMessage('must be at greatest 30 chars long'),

  body('payment_name')
    .isString()
    .isLength({ min: 1 }).withMessage('must be at least 1 chars long')
    .isLength({ max: 30 }).withMessage('must be at greatest 30 chars long'),

  body('invoice_number')
    .isString()
    .isLength({ min: 1 }).withMessage('must be at least 1 chars long')
    .isLength({ max: 100 }).withMessage('must be at greatest 30 chars long')
],
(req, res) => {
  const response = defResponse
  response.response.data = {}
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    response.status = 'failed'
    response.error.from = 'payment_service'
    response.error.code = 'error_on_' + errors.array()[0].param
    response.error.message = errors.array()[0].msg
    response.response = {}
    return res.status(422).json(response)
  }
  Helper.detailInvoice(req.body, res, response)
})
app.post('/cancelInvoice2', [
  body('brand_name')
    .isString()
    .isLength({ min: 1 }).withMessage('must be at least 1 chars long')
    .isLength({ max: 30 }).withMessage('must be at greatest 30 chars long'),

  body('payment_name')
    .isString()
    .isLength({ min: 1 }).withMessage('must be at least 1 chars long')
    .isLength({ max: 30 }).withMessage('must be at greatest 30 chars long'),

  body('invoice_number')
    .isString()
    .isLength({ min: 1 }).withMessage('must be at least 1 chars long')
    .isLength({ max: 100 }).withMessage('must be at greatest 30 chars long')

],
(req, res) => {
  const response = defResponse
  response.response.data = {}
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    response.status = 'failed'
    response.error.from = 'payment_service'
    response.error.code = 'error_on_' + errors.array()[0].param
    response.error.message = errors.array()[0].msg
    response.response = {}
    return res.status(422).json(response)
  }
  Helper.cancelInvoice(req.body, res, response)
}
)
app.post('/getToken2', [
  body('brand_name')
    .isString()
    .isLength({ min: 1 }).withMessage('must be at least 1 chars long')
    .isLength({ max: 30 }).withMessage('must be at greatest 30 chars long'),

  body('payment_name')
    .isString()
    .isLength({ min: 1 }).withMessage('must be at least 1 chars long')
    .isLength({ max: 30 }).withMessage('must be at greatest 30 chars long'),

  body('code')
    .isString()
    .isLength({ min: 1 }).withMessage('must be at least 1 chars long')
    .isLength({ max: 100 }).withMessage('must be at greatest 100 chars long')

],
(req, res) => {
  const response = defResponse
  response.response.data = {}
  response.error = {}
  response.response.data = {}
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    response.status = 'failed'
    response.error.from = 'payment_service'
    response.error.code = 'error_on_' + errors.array()[0].param
    response.error.message = errors.array()[0].msg
    response.response = {}
    return res.status(422).json(response)
  }
  if (req.body.payment_name === 'lendmn') {
    Helper.getToken(req.body, res, response)
  } else {
    response.status = 'failed'
    response.error.code = 'can_not_get_token_on_' + req.body.payment_name
    response.error.message = 'getToken үйлдэл хийх боломжгүй'
    res.json(response)
  }
}
)
app.post('/userInfo2', [
  body('brand_name')
    .isString()
    .isLength({ min: 1 }).withMessage('must be at least 1 chars long')
    .isLength({ max: 30 }).withMessage('must be at greatest 30 chars long'),

  body('payment_name')
    .isString()
    .isLength({ min: 1 }).withMessage('must be at least 1 chars long')
    .isLength({ max: 30 }).withMessage('must be at greatest 30 chars long'),

  body('token')
    .isString()
    .isLength({ min: 1 }).withMessage('must be at least 1 chars long')
    .isLength({ max: 100 }).withMessage('must be at greatest 100 chars long')

],
(req, res) => {
  const response = defResponse
  response.response.data = {}
  response.error = {}
  response.response.data = {}
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    response.status = 'failed'
    response.error.from = 'payment_service'
    response.error.code = 'error_on_' + errors.array()[0].param
    response.error.message = errors.array()[0].msg
    response.response = {}
    return res.status(422).json(response)
  }
  if (req.body.payment_name === 'lendmn') {
    Helper.userInfo(req.body, res, response)
  } else {
    response.status = 'failed'
    response.error.code = 'can_not_get_token_on_' + req.body.payment_name
    response.error.message = 'userInfo үйлдэл хийх боломжгүй'
    res.json(response)
  }
}
)
app.get('/hook/qpay', async (req, res) => {
  const response = defResponse
  const invoices = await Invoice.findAll({
    where: {
      invoice_number: req.query.invoiceId
    }
  })
  if (invoices.length > 0) {
    const headers = {
      'Content-Type': 'application/json'
    }
    const url = 'https://api.qpay.mn/v1/'
    const invoiceData = invoices[0]
    const paymentList = await PaymentList.findAll({
      where: {
        brand_name: invoiceData.brand_name,
        payment_name: invoiceData.payment_name
      }
    })
    const credential = JSON.parse(paymentList[0].config)
    console.log('invoiceData', invoiceData)
    headers.Authorization = 'Bearer ' + invoiceData.access_token

    const body = {
      merchant_id: credential.merchant_code,
      bill_no: req.query.invoiceId
    }
    axios.post(url + 'bill/check/', body, { headers: headers }).then((res1) => {
      if (res1.data.payment_info.payment_status === 'PAID') {
        let tempDescription = invoiceData.description
        axios.get('https://us-central1-toktok-1df1f.cloudfunctions.net/hook/qpay?invoice_number=' + req.query.invoiceId + '&description=' + (tempDescription ? tempDescription : '')).then(funcRes => {
          console.log('firebase hook', funcRes.data)
          Invoice.update({
            status: res1.data.payment_info.payment_status
          },
          { where: { id: invoiceData.id } }).then((sqlRes) => {
            if (body) {
              response.response.data = 'success'
              res.send(response)
            } else {
              response.status = 'failed'
              res.send(response)
            }
          })
        })
      } else {
        response.status = 'success'
        response.response = 'all ready'
        res.json(response)
      }
    })
  } else {
    res.send({ from: 'payment', code: 'not_found', message: 'Нэхэмжлэл олдсонгүй.' })
  }
})
app.post('/hook/mongolChat', async (req, res) => {
  const response = defResponse
  const invoice = await Invoice.findOne({
    where: {
      invoice_number: req.body.data.reference_number
    }
  })
  if (invoice) {
    const paymentList = await PaymentList.findOne({
      where: {
        brand_name: invoice.brand_name,
        payment_name: invoice.payment_name
      }
    })
    const credential = JSON.parse(paymentList.config)
    const headers = {
      'Content-Type': 'application/json',
      Authorization: credential.Authorization,
      'api-key': credential.apiKey
    }
    const resAxios = await axios.post(credential.url + 'onlineqr/status', { qr: invoice.qr_code }, { headers: headers })
    Invoice.update({
      status: resAxios.data.status.toUpperCase()
    },
    { where: { id: invoice.id } }).then(async () => {
      let tempDescription = (await Invoice.findOne({where: { id: invoice.id }})).description
      axios.get('https://us-central1-toktok-1df1f.cloudfunctions.net/hook/qpay?invoice_number=' + req.body.data.reference_number + '&description=' + (tempDescription ? tempDescription : '')).then(funcRes => {
        console.log('firebase hook', funcRes.data)
        if (funcRes.data) {
          response.response.data = 'success'
          res.send(response)
        } else {
          response.status = 'failed'
          res.send(response)
        }
      })
    })
  } else {
    res.send({ from: 'payment', code: 'not_found', message: 'Нэхэмжлэл олдсонгүй.' })
  }
})
app.listen(port, () => console.log(`Example app listening on port ${port}!`))
