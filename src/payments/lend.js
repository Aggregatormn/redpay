const axios = require('axios')
const responseHandler = require('../responseHandlerNew')
const querystring = require('querystring')
module.exports = {
  createInvoice: async function (data, credential) {
    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'x-and-auth-token': credential.LEND_TOKEN
    }
    const body = {
      amount: parseInt(data.amount),
      duration: credential.INVOICE_DURATION,
      description: data.description,
      trackingData: data.trackingData,
      successUri: credential.SUCCESS_URI
    }
    const url = 'https://mgw.lend.mn/api/w/invoices'
    // let url = 'https://mgw.test.lending.mn/api/w/invoices';

    const promise = new Promise((resolve, reject) => {
      axios.post(url, querystring.stringify(body), { headers: headers }).then((res) => {
        if (res.data.code === 0) {
          resolve(responseHandler.successHandler(res.data, data.payment_name, data.brand_name))
        } else {
          resolve(responseHandler.errorHandler(res.data, data.payment_name, res.status, res.statusText))
        }
      }).catch((err) => {
        reject(responseHandler.criticErrorHandler(err, data.payment_name))
      })
    })
    const result = await promise
    return result
  },
  detailInvoice: async function (data, credential) {
    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'x-and-auth-token': credential.LEND_TOKEN
    }
    const url = 'https://mgw.lend.mn/api/w/invoices/' + data.invoice_number
    // let url = 'https://mgw.test.lending.mn/api/w/invoices/' + data.invoice_number
    const promise = new Promise((resolve, reject) => {
      axios.get(url, { headers: headers }).then((res) => {
        if (res.data.code === 0) {
          resolve(responseHandler.detailHandler(res.data, data.payment_name))
        } else {
          reject(responseHandler.errorHandler(res.data, data.payment_name, res.status, res.statusText))
        }
      }).catch((err) => {
        reject(responseHandler.errorHandler(err.response.data, data.payment_name, err.response.status, err.response.statusText))
      })
    })
    const result = await promise
    return result
  },
  cancelInvoice: async function (data, credential) {
    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'x-and-auth-token': credential.LEND_TOKEN
    }
    const url = 'https://mgw.lend.mn/api/w/invoices/' + data.invoice_number
    // let url = 'https://mgw.test.lending.mn/api/w/invoices/' + data.invoice_number
    const promise = new Promise((resolve, reject) => {
      axios.delete(url, { headers: headers }).then((res) => {
        if (res.data.code === 0) {
          resolve(responseHandler.cancelHandler(res.data, data.payment_name))
        } else {
          reject(responseHandler.errorHandler(res.data, data.payment_name, res.status, res.statusText))
        }
      }).catch((err) => {
        reject(responseHandler.errorHandler(err, data.type))
      })
    })
    const result = await promise
    return result
  },
  getToken: async function (data, credential) {
    console.log(credential)
    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
    const body = {
      code: data.code,
      client_id: credential.LEND_CLIENT_ID,
      client_secret: credential.LEND_CLIENT_SECRET,
      grant_type: 'authorization_code',
      redirect_uri: credential.REDIRECT_URI
    }
    const url = 'https://mgw.lend.mn/api/oauth/v2/token'
    // let url = 'https://mgw.test.lending.mn/api/w/invoices';

    const promise = new Promise((resolve, reject) => {
      axios.post(url, querystring.stringify(body), { headers: headers }).then((res) => {
        if (res.data.code === 0) {
          const temp = {}
          temp.access_token = res.data.response.accessToken
          temp.expires_in = res.data.response.expiresIn
          temp.scope = res.data.response.scope
          resolve(temp)
        } else {
          reject(responseHandler.errorHandler(res.data, data.payment_name, res.status, res.statusText))
        }
      }).catch((err) => {
        reject(responseHandler.criticErrorHandler(err, data.payment_name))
      })
    })
    const result = await promise
    return result
  },
  userInfo: async function (data, credential) {
    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'x-and-auth-token': data.token
    }
    const url = 'https://mgw.lend.mn/api/user/info'
    // let url = 'https://mgw.test.lending.mn/api/user/info';

    const promise = new Promise((resolve, reject) => {
      axios.get(url, { headers: headers }).then((res) => {
        if (res.data.code === 0) {
          const temp = {}
          temp.user_id = res.data.response.userId
          temp.first_name = res.data.response.firstName
          temp.last_name = res.data.response.lastName
          temp.phone_number = res.data.response.phoneNumber
          resolve(temp)
        } else {
          reject(responseHandler.errorHandler(res.data, data.payment_name, res.status, res.statusText))
        }
      }).catch((err) => {
        reject(responseHandler.criticErrorHandler(err, data.payment_name))
      })
    })
    const result = await promise
    return result
  }
}
