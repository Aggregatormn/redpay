const axios = require('axios')
const Invoice = require('../model/invoice')
const responseHandler = require('../responseHandlerNew')
module.exports = {
  createInvoice: async function (data, credential) {
    // const invoices = await Invoice.findAll({ where: { invoice_number: data.trackingData } })
    // if(invoices.length>0) {
    //   return (responseHandler.successHandler(invoices[0], data.payment_name, data.brand_name))
    // } else {
    const headers = {
      'Content-Type': 'application/json'
    }
    const authBody = {
      client_id: credential.client_id,
      client_secret: credential.client_secret,
      grant_type: 'client',
      refresh_token: ''
    }
    const body = {
      template_id: credential.invoice_code,
      merchant_id: credential.merchant_code,
      branch_id: '1',
      pos_id: '1',
      receiver: {
        id: 'CUST_001',
        register_no: 'ddf',
        name: 'Central brnach',
        email: 'info@info.mn',
        phone_number: '99888899',
        note: 'davaa'
      },
      bill_no: data.trackingData,
      date: new Date(),
      description: data.description,
      amount: data.amount,
      btuk_code: '',
      vat_flag: '0'
    }
    const url = 'https://api.qpay.mn/v1/'

    const promise = new Promise((resolve, reject) => {
      axios.post(url + 'auth/token', authBody, { headers: headers }).then((res) => {
        headers.Authorization = 'Bearer ' + res.data.access_token
        axios.post(url + 'bill/create', body, { headers: headers }).then(async (res1) => {
          await Invoice.create({
            payment_id: res1.data.payment_id,
            amount: data.amount,
            brand_name: data.brand_name,
            payment_name: data.payment_name,
            invoice_number: data.trackingData,
            description: data.description,
            qr_code: res1.data.qPay_QRcode,
            invoice_url: null,
            status: 'created',
            access_token: res.data.access_token
          })
          resolve(responseHandler.successHandler(res1.data, data.payment_name, data.brand_name))
        }).catch((err) => {
          reject(err)
        })
      }).catch((err) => {
        reject(err)
      })
    })
    const result = await promise
    return result
    // }
  },
  detailInvoice: async function (data, credential) {
    const headers = {
      'Content-Type': 'application/json'
    }
    const url = 'https://api.qpay.mn/v1/'
    const invoices = await Invoice.findAll({
      where: {
        invoice_number: data.invoice_number, payment_name: data.payment_name, brand_name: data.brand_name
      }
    })
    if (invoices.length > 0) {
      const invoiceData = invoices[0]

      headers.Authorization = 'Bearer ' + invoiceData.access_token

      const body = {
        merchant_id: credential.merchant_code,
        bill_no: data.invoice_number
      }
      const promise = new Promise((resolve, reject) => {
        axios.post(url + 'bill/check/', body, { headers: headers }).then((res1) => {
          const response = responseHandler.detailHandler(res1.data, data.payment_name)
          response.qr_code = invoiceData.qr_code
          Invoice.update({
            status: res1.data.payment_info.payment_status
          },
          { where: { id: invoiceData.id } }).then((sqlRes) => {
            resolve(response)
          })
        }).catch((err) => {
          reject(responseHandler.errorHandler(err.response.data, data.payment_name, err.response.status, err.response.statusText))
        })
      })
      const result = await promise
      return result
    } else {
      return ({ from: 'payment', code: 'not_found', message: 'Нэхэмжлэл олдсонгүй.' })
    }
  }
}
